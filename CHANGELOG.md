# Changelog
All Notable changes to `oauth2-meetup` will be documented in this file


## 0.1.0 - 2018-02-12

### Added
- Initial release!

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing
