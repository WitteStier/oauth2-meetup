Meetup Provider for OAuth 2.0 Client
====================================

[![pipeline status](https://gitlab.com/WitteStier/oauth2-meetup/badges/master/pipeline.svg)](https://gitlab.com/WitteStier/oauth2-meetup/commits/master)
[![coverage report](https://gitlab.com/WitteStier/oauth2-meetup/badges/master/coverage.svg)](https://gitlab.com/WitteStier/oauth2-meetup/commits/master)

This package provides Meetup.com OAuth 2.0 support for the PHP League's [OAuth 2.0 Client](https://github.com/thephpleague/oauth2-client).


## Requirements

The following versions of PHP are supported.

* PHP 7.1
* PHP 7.2


## Installation

To install, use composer:

``` bash
composer require wittestier/oauth2-meetup
```


## Usage

### Authorization Code Flow

``` php
$provider = new \WitteStier\OAuth2\Client\Provider\Meetup([
    'clientId' => '{meetup-consumer-key}',
    'clientSecret' => '{meetup-consumer-secret}',
    'redirectUri' => '{meetup-consumer-redirect-uri}',
]);

// If we don't have an authorization code then get one.
if (isset($_GET['code']) === false) {

    // Fetch the authorization URL from the provider; this returns the urlAuthorize option and generates and applies 
    // any necessary parameters (e.g. state).
    $authorizationUrl = $provider->getAuthorizationUrl();

    // Get the state generated for you and store it to the session.
    $_SESSION['oauth2state'] = $provider->getState();

    // Redirect the user to the authorization URL.
    header('Location: ' . $authorizationUrl);
    exit;

// Check given state against previously stored one to mitigate CSRF attack.
} elseif (empty($_GET['state']) === true || ($_GET['state'] !== $_SESSION['oauth2state'])) {

    unset($_SESSION['oauth2state']);
    exit('Invalid state');

} else {

    try {

        // Try to get an access token using the authorization code grant.
        $grant = new League\OAuth2\Client\Grant\AuthorizationCode();
        $accessToken = $provider->getAccessToken($grant, [
            'code' => $_GET['code']
        ]);

    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

        // Failed to get the access token or user details.
        exit($e->getMessage());

    }

    // We have an access token, which we may use in authenticated requests against the service provider's API.
    echo $accessToken->getToken() . "\n";
    echo $accessToken->getRefreshToken() . "\n";
    echo $accessToken->getExpires() . "\n";
    echo ($accessToken->hasExpired() ? 'expired' : 'not expired') . "\n";

    try {

        // Using the access token, we may look up details about the resource owner.
        $resourceOwner = $provider->getResourceOwner($accessToken);

        var_export($resourceOwner->toArray());

   } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

        // Failed to get the access token or user details.
        exit($e->getMessage());

    }

}

```

### Managing Scopes

When creating your Meetup authorization URL, you can specify the state and scopes your application may authorize.

``` php
$options = [
    'state' => 'OPTIONAL_CUSTOM_CONFIGURED_STATE',
    'scope' => [ 'ageless', 'basic', 'event_management', 'group_edit', 'group_content_edit', 'group_join', 'messaging', 'profile_edit', 'reporting', 'rsvp' ],
];

$authorizationUrl = $provider->getAuthorizationUrl($options);
```

If neither are defined, the provider will utilize internal defaults.

At the time of authoring this documentation, the [following scopes are available.](https://www.meetup.com/meetup_api/auth/#oauth2-scopes)

### Refreshing a Token

``` php
$provider = new \WitteStier\OAuth2\Client\Provider\Meetup([
    'clientId' => '{meetup-consumer-key}',
    'clientSecret' => '{meetup-consumer-secret}',
    'redirectUri' => '{meetup-consumer-redirect-uri}',
]);

// Fetch your token from a datastore.
$token = fetchAccessToken();

$refreshToken = $token->getRefreshToken();

if ($token->hasExpired() === true) {

    $grant = new League\OAuth2\Client\Grant\RefreshToken();
    $newAccessToken = $provider->getAccessToken($grant, [
        'refresh_token' => $token->getRefreshToken()
    ]);

    // Purge old access token and store new access token to your data store.

}

```


## Testing

``` bash
$ ./vendor/bin/phpunit
# Or
$ composer test
```

``` bash
$ ./vendor/bin/phpcs src --standard=psr2 -sp
# Or
$ composer check
```


## Contributing

Please see [CONTRIBUTING](https://gitlab.com/WitteStier/oauth2-meetup/blob/master/CONTRIBUTING.md) for details.


## Credits

- [WitteStier](https://gitlab.com/WitteStier)
- [All Contributors](https://gitlab.com/WitteStier/oauth2-meetup/graphs/master)


## License

The MIT License (MIT). Please see [License File](https://gitlab.com/WitteStier/oauth2-meetup/blob/master/LICENSE) for more information.