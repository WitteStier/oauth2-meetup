<?php

declare(strict_types=1);

namespace WitteStier\OAuth2\Client\Provider;

use GuzzleHttp\ClientInterface;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class MeetupTest extends TestCase
{
    /**
     * @var \League\OAuth2\Client\Provider\AbstractProvider
     */
    private $provider;

    protected function setUp()
    {
        $this->provider = new \WitteStier\OAuth2\Client\Provider\Meetup([
            'clientId' => 'mock_client_id',
            'clientSecret' => 'mock_secret',
            'redirectUri' => 'none',
        ]);
    }

    public function test_AuthorizationUrl_query_parameters()
    {
        $url = $this->provider->getAuthorizationUrl();
        parse_str(parse_url($url, PHP_URL_QUERY), $query);

        $this->assertArrayHasKey('client_id', $query);
        $this->assertEquals('mock_client_id', $query['client_id']);

        $this->assertArrayHasKey('response_type', $query);
        $this->assertEquals('code', $query['response_type']);

        $this->assertArrayHasKey('redirect_uri', $query);
        $this->assertEquals('none', $query['redirect_uri']);

        $this->assertArrayHasKey('scope', $query);
        $this->assertEquals('', $query['scope']);

        $this->assertArrayHasKey('state', $query);
        $this->assertNotEmpty($query['state']);

        $this->assertArrayHasKey('approval_prompt', $query);
    }

    public function test_getBaseAuthorizationUrl_returns_authorize_url()
    {
        $result = $this->provider->getBaseAuthorizationUrl();

        $this->assertEquals('https://secure.meetup.com/oauth2/authorize', $result);
    }

    public function test_getBaseAccessTokenUrl_returns_access_token_url()
    {
        $result = $this->provider->getBaseAccessTokenUrl([]);

        $this->assertEquals('https://secure.meetup.com/oauth2/access', $result);
    }

    public function test_getResourceOwnerDetailsUrl_returns_resource_owner_detail_url()
    {
        $accessTokenMock = new AccessToken([
            'access_token' => 'token_123',
        ]);

        $result = $this->provider->getResourceOwnerDetailsUrl($accessTokenMock);
        parse_str(parse_url($result, PHP_URL_QUERY), $query);

        $this->assertArrayHasKey('access_token', $query);
        $this->assertEquals('token_123', $query['access_token']);

        $this->assertStringStartsWith('https://api.meetup.com/2/member/self?', $result);
    }

    public function test_getResourceOwner_returns_instance_of_ResourceOwner()
    {
        $responseMock = $this->getMockForAbstractClass(ResponseInterface::class);
        $responseMock->method('getBody')->willReturn('{"id":123}');
        $responseMock->method('getHeader')->willReturn(['content-type' => 'json']);
        $responseMock->method('getStatusCode')->willReturn(200);

        $clientMock = $this->getMockForAbstractClass(ClientInterface::class);
        $clientMock->method('send')->willReturn($responseMock);

        $this->provider->setHttpClient($clientMock);

        $accessTokenMock = new AccessToken([
            'access_token' => 'token_123',
        ]);

        $result = $this->provider->getResourceOwner($accessTokenMock);

        $this->assertInstanceOf(ResourceOwner::class, $result);
    }

    public function test_getResourceOwner_throws_IdentityProviderException_for_no_ok_status()
    {
        $responseMock = $this->getMockForAbstractClass(ResponseInterface::class);
        $responseMock->method('getBody')->willReturn('{}');
        $responseMock->method('getHeader')->willReturn(['content-type' => 'json']);

        $responseMock->method('getStatusCode')->willReturn(400); // Or any other status code except 200

        $clientMock = $this->getMockForAbstractClass(ClientInterface::class);
        $clientMock->method('send')->willReturn($responseMock);

        $this->provider->setHttpClient($clientMock);

        $accessTokenMock = new AccessToken([
            'access_token' => 'token_123',
        ]);

        $this->expectException(IdentityProviderException::class);
        $this->expectExceptionMessage('Invalid response status');

        $this->provider->getResourceOwner($accessTokenMock);
    }

    public function test_getResourceOwner_throws_IdentityProviderException_for_invalid_json_response()
    {
        $responseMock = $this->getMockForAbstractClass(ResponseInterface::class);
        $responseMock->method('getBody')->willReturn('Oops. This is not JSON.');
        $responseMock->method('getHeader')->willReturn(['content-type' => 'bogus']);

        $clientMock = $this->getMockForAbstractClass(ClientInterface::class);
        $clientMock->method('send')->willReturn($responseMock);

        $this->provider->setHttpClient($clientMock);

        $accessTokenMock = new AccessToken([
            'access_token' => 'token_123',
        ]);

        $this->expectException(IdentityProviderException::class);
        $this->expectExceptionMessage('Invalid response body');

        $this->provider->getResourceOwner($accessTokenMock);
    }
}
