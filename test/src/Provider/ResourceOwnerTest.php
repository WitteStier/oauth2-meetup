<?php

declare(strict_types=1);

namespace WitteStier\OAuth2\Client\Provider;

use PHPUnit\Framework\TestCase;

class ResourceOwnerTest extends TestCase
{
    private $mockData = [];

    protected function setUp()
    {
        $this->mockData = [
            'id' => 123,
            'link' => 'https://link',
            'name' => 'name abc',
            'birthday' => [
                'year' => 1970,
            ],
            'photo' => [
                'photo_link' => 'https://photo',
            ],
            'status' => 'status',
            'joined' => 123,
            'visited' => 456,
            'lang' => 'lang en',
            'country' => 'country ?',
            'city' => 'city ?',
            'lat' => 1.2,
            'lon' => -1.2,
            'topics' => [
                'foo' => [],
            ],
            'otherServices' => [
                'bar' => [],
            ],
            'self' => [
                'baz' => [],
            ],
        ];
    }

    public function test_injected_data_can_be_retrieved_with_getters()
    {
        $result = new ResourceOwner(...array_values($this->mockData));

        $this->assertEquals($this->mockData['id'], $result->getId());
        $this->assertEquals($this->mockData['link'], $result->getLink());
        $this->assertEquals($this->mockData['name'], $result->getName());
        $this->assertEquals($this->mockData['birthday'], $result->getBirthday());
        $this->assertEquals($this->mockData['photo'], $result->getPhoto());
        $this->assertEquals($this->mockData['status'], $result->getStatus());
        $this->assertEquals($this->mockData['joined'], $result->getJoined());
        $this->assertEquals($this->mockData['visited'], $result->getVisited());
        $this->assertEquals($this->mockData['lang'], $result->getLang());
        $this->assertEquals($this->mockData['country'], $result->getCountry());
        $this->assertEquals($this->mockData['city'], $result->getCity());
        $this->assertEquals($this->mockData['lat'], $result->getLat());
        $this->assertEquals($this->mockData['lon'], $result->getLon());
        $this->assertEquals($this->mockData['topics'], $result->getTopics());
        $this->assertEquals($this->mockData['otherServices'], $result->getOtherServices());
        $this->assertEquals($this->mockData['self'], $result->getSelf());
    }

    public function test_fromArray_returns_instance_of_ResourceOwner()
    {
        $resourceOwner = new ResourceOwner(...array_values($this->mockData));

        $result = ResourceOwner::fromArray($this->mockData);

        $this->assertInstanceOf(ResourceOwner::class, $result);

        $this->assertEquals($resourceOwner, $result);
    }

    public function test_fromArray_accepts_CamelCase_and_Underscore_data_keys()
    {
        $data = $this->mockData;

        $data['other_services'] = $data['otherServices'];
        unset($data['otherServices']);

        $result = ResourceOwner::fromArray($data);

        $this->assertEquals($data['other_services'], $result->getOtherServices());
    }

    public function test_fromArray_uses_empty_defaults()
    {
        $result = ResourceOwner::fromArray([]);

        $this->assertEquals(0, $result->getId());
        $this->assertEquals('', $result->getLink());
        $this->assertEquals('', $result->getName());
        $this->assertEquals([], $result->getBirthday());
        $this->assertEquals([], $result->getPhoto());
        $this->assertEquals('', $result->getStatus());
        $this->assertEquals(0, $result->getJoined());
        $this->assertEquals(0, $result->getVisited());
        $this->assertEquals('', $result->getLang());
        $this->assertEquals('', $result->getCountry());
        $this->assertEquals('', $result->getCity());
        $this->assertEquals(0.0, $result->getLat());
        $this->assertEquals(0.0, $result->getLon());
        $this->assertEquals([], $result->getTopics());
        $this->assertEquals([], $result->getOtherServices());
        $this->assertEquals([], $result->getSelf());
    }

    public function test_toArray_returns_array_with_all_data()
    {
        $result = new ResourceOwner(...array_values($this->mockData));

        $result = $result->toArray();

        $this->assertEquals($this->mockData, $result);
    }
}
